Prometheus VarnishKafka Exporter
===

A Prometheus exporter daemon that exports metrics from varnishkafka logs.

Configuring
---
```
stats_files [list]: List of files to scrape metrics from.  Default: "[]"
num_entries_to_get [int]: Number of entries from the stats_files to read.  Default: "2"
required_entries [list]: List of entry keys that must be read before rendering metrics.  Default: "[]"
stats [dict]: Dict of stats mappings.  Default: "{}"
```

####Note: the `source` label is hardcoded from the arbitrary value of the first part of the filename split on `.`.

Labels search backwards up the tree for an instance of themselves or their configured key denoted by `<labelname>=<key>`.
For example, we want client_id and the broker name as labels:
```json
{
  "kafka": {
    "client_id": "varnishkafka#producer-1",
    "brokers": {
      "broker_1": {
        "name": "broker_1",
        "metric_1": 1,
        "metric_2": 2
      }
    }
  }
}
```
would want to be consumed by config:
```yaml
stats_files:
  - 'examplesource.stats.json'
stats:
  kafka:
    'brokers':
      'metric_1':
        type: 'GaugeMetricFamily'
        'name': 'varnishkafka_brokers_metric_one'
        'description': 'Demo'
        'labels':  ['client_id', 'broker=name']
```
and yield in the metric labels:
```json
{
  "client_id": "varnishkafka#producer-1",
  "broker": "broker_1"
}
```

See `example.config.yaml` and the json files in the `sample` directory for a working example.  

Usage
---
```
usage: prometheus_varnishkafka_exporter.py [-h] [-l ADDRESS] [-d] [-c FILE]
                                           [--show-config-options]

optional arguments:
  -h, --help            show this help message and exit
  -l ADDRESS, --listen ADDRESS
                        Listen on this address
  -d, --debug           Enable debug logging
  -c FILE, --configfile FILE
                        Config file in yaml format
  --show-config-options
                        Prints configuration options and exits.
```
