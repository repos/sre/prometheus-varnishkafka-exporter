import unittest
from copy import copy
from argparse import Namespace
from prometheus_client.core import GaugeMetricFamily
from prometheus_varnishkafka_exporter import (
    PrometheusVarnishKafkaExporter,
    get_config,
    CONFIG_DEFAULTS,
    validate_config
)

# TODO: Add black code formatter test case (when package available)


class TestExporter(unittest.TestCase):
    def setUp(self):
        self.config_file = 'example.config.yaml'

    def test_extract_source(self):
        filename = '/var/cache/varnishkafka/eventlogging.stats.json'
        self.assertEqual(
            PrometheusVarnishKafkaExporter(get_config(self.config_file)).extract_source(filename),
            'eventlogging'
        )

    def test_populate_metrics_one_level(self):
        exporter = PrometheusVarnishKafkaExporter(get_config(self.config_file))
        stats = {
            'metric_one': 1,
            'metric_two': 2,
            'name': 'test'
        }
        metrics = {
            'metric_one': {
                'type': 'GaugeMetricFamily',
                'name': 'varnishkafka_metric_one',
                'description': 'M1',
                'labels': ['name']
            },
            'metric_two': {
                'type': 'GaugeMetricFamily',
                'name': 'varnishkafka_metric_two',
                'description': 'M2',
                'labels': ['name']
            }
        }
        expected = {
            'varnishkafka_metric_one': {
                'type': GaugeMetricFamily,
                'description': 'M1',
                'labels': {'source': 'test_source', 'name': 'test'},
                'value': 1.0
            },
            'varnishkafka_metric_two': {
                'type': GaugeMetricFamily,
                'description': 'M2',
                'labels': {'source': 'test_source', 'name': 'test'},
                'value': 2.0
            }
        }
        exporter._populate_metrics(stats, metrics, [{'source': 'test_source'}])
        for metric in exporter.metrics:
            self.assertIsInstance(metric, expected.get(metric.name)['type'])
            self.assertIsNotNone(expected.get(metric.name))
            if hasattr(metric.samples[0], 'labels'):
                name, labels, value = metric.name, metric.samples[0].labels, metric.samples[0].value
            else:
                name, labels, value = metric.samples[0]
            self.assertEqual(expected.get(name)['description'], metric.documentation)
            self.assertEqual(expected.get(name)['labels'], labels)
            self.assertEqual(expected.get(name)['value'], value)

    def test_populate_metrics_multilevel(self):
        exporter = PrometheusVarnishKafkaExporter(get_config(self.config_file))
        stats = {
            'metric_one': 1,
            'name': 'test_one',
            'metric_two': {
                'subs': {
                    'sub_one': {
                        'submetric_one': 2,
                        'submetric_two': 3,
                        'name': 'test_sub_one'
                    },
                    'sub_two': {
                        'submetric_three': 4,
                        'submetric_four': 5,
                        'name': 'test_sub_two'
                    }
                }
            }
        }
        metrics = {
            'metric_one': {
                'type': 'GaugeMetricFamily',
                'name': 'varnishkafka_metric_one',
                'description': 'M1',
                'labels': ['name']
            },
            'metric_two': {
                'label=subs': {
                    'submetric_one': {
                        'type': 'GaugeMetricFamily',
                        'name': 'varnishkafka_submetric_one',
                        'description': 'SM1',
                        'labels': ['name']
                    },
                    'submetric_two': {
                        'type': 'GaugeMetricFamily',
                        'name': 'varnishkafka_submetric_two',
                        'description': 'SM2',
                        'labels': ['name']
                    }
                }
            }
        }
        expected = {
            'varnishkafka_metric_one': {
                'type': GaugeMetricFamily,
                'description': 'M1',
                'labels': {'source': 'test_source', 'name': 'test_one'},
                'value': 1
            },
            'varnishkafka_submetric_one': {
                'type': GaugeMetricFamily,
                'description': 'SM1',
                'labels': {'source': 'test_source', 'name': 'test_sub_one'},
                'value': 2
            },
            'varnishkafka_submetric_two': {
                'type': GaugeMetricFamily,
                'description': 'SM2',
                'labels': {'source': 'test_source', 'name': 'test_sub_one'},
                'value': 3
            },
            'varnishkafka_submetric_three': {
                'type': GaugeMetricFamily,
                'description': 'SM3',
                'labels': {'source': 'test_source', 'name': 'test_sub_two'},
                'value': 4
            },
            'varnishkafka_submetric_four': {
                'type': GaugeMetricFamily,
                'description': 'SM4',
                'labels': {'source': 'test_source', 'name': 'test_sub_two'},
                'value': 5
            }
        }
        exporter._populate_metrics(stats, metrics, [{'source': 'test_source'}])
        for metric in exporter.metrics:
            self.assertIsInstance(metric, expected.get(metric.name)['type'])
            self.assertIsNotNone(expected.get(metric.name))
            if hasattr(metric.samples[0], 'labels'):
                name, labels, value = metric.name, metric.samples[0].labels, metric.samples[0].value
            else:
                name, labels, value = metric.samples[0]
            self.assertEqual(expected.get(name)['description'], metric.documentation)

            self.assertEqual(expected.get(name)['labels'], labels)
            self.assertEqual(expected.get(name)['value'], value)

    def test_extract_label(self):
        self.assertEqual(
            ('test', 'test'),
            PrometheusVarnishKafkaExporter.extract_label('test')
        )
        self.assertEqual(
            ('key', 'value'),
            PrometheusVarnishKafkaExporter.extract_label('key=value')
        )

    def test_validate_config(self):
        for value in CONFIG_DEFAULTS.values():
            self.assertNotEqual(
                value['type'],
                type(None),
                'NoneType cannot be used as a config option type'
                ' because None is used to detect when a default should be used'
            )
        valid_config = Namespace()
        [setattr(valid_config, key, value['default']) for key, value in CONFIG_DEFAULTS.items()]
        self.assertIsNone(
            validate_config(valid_config),
            'A valid config should return None and not raise an Exception'
        )

        for key in CONFIG_DEFAULTS.keys():
            invalid_config = copy(valid_config)
            setattr(invalid_config, key, None)  # None is not acceptable for any config option
            with self.assertRaises(TypeError, msg='Invalid types should raise TypeError'):
                validate_config(invalid_config)

        with self.assertRaises(ValueError, msg='num_entries_to_get < required_entries should raise ValueError'):
            invalid_config = copy(valid_config)
            invalid_config.num_entries_to_get = 2
            invalid_config.required_entries = ['a', 'b', 'c']
            validate_config(invalid_config)

    def test_default_config_attributes(self):
        for key, value in CONFIG_DEFAULTS.items():
            self.assertIsNotNone(
                value.get('type'),
                'Configuration default for "{}" option must have a type'.format(key)
            )
            self.assertIsNotNone(
                value.get('description'),
                'Configuration default for "{}" option must have a description'.format(key)
            )
            self.assertIsNotNone(
                value.get('default'),
                'Configuration default for "{}" option must have a sane default'.format(key)
            )

    def test_reader(self):
        class ReaderMock:
            lines = ['{"a": "1", "b": "2"}' for _ in range(0, 10)]

            def __init__(self, filename, encoding):
                pass

            def __enter__(self):
                return self

            def __exit__(self, exc_type, exc_val, exc_tb):
                pass

            def __iter__(self) -> list:
                for line in self.lines:
                    yield line

        count = 2
        self.assertEqual(
            count,
            len(
                PrometheusVarnishKafkaExporter(
                    get_config(self.config_file)
                ).get_entries('filename', count, 'encoding', ReaderMock)
            )
        )

    def test_get_labels(self):
        # this is a bit convoluted because python3.5 messes with list order
        # the goal is to keep name: value zip()-able
        trail = [
            {'source': 'test_source'},
            {'name': 'test_broker'},
            {'topic': 'test_topic'}
        ]
        labels = ['source', 'broker=name', 'topic']
        actual_label_names, actual_label_values = PrometheusVarnishKafkaExporter(
            get_config(self.config_file)
        ).get_labels(trail, labels)
        for i in range(0, len(actual_label_names)):
            self.assertEqual(
                'test_' + actual_label_names[i].split('=')[0],
                actual_label_values[i]
            )
