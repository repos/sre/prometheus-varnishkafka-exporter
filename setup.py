from setuptools import setup

REQUIRED_PACKAGES = [
    'prometheus-client',
    'file-read-backwards',
    'pyyaml',
]

with open('README.md', 'r') as f:
    long_description = f.read()

setup(
    name='prometheus-varnishkafka-exporter',
    version='0.1',
    description='Prometheus Exporter for VarnishKafka metrics',
    long_description=long_description,
    author='Cole White',
    author_email='cwhite@wikimedia.org',
    python_requires='>=3.5.0',
    url='https://gerrit.wikimedia.org/r/plugins/gitiles/operations/debs/prometheus-varnishkafka-exporter/',
    packages=['prometheus_varnishkafka_exporter'],
    entry_points={
        'console_scripts': [
            'prometheus-varnishkafka-exporter = prometheus_varnishkafka_exporter:main'
        ]
    },
    data_files=[('share/prometheus-varnishkafka-exporter', ['example.config.yaml'])],
    install_requires=REQUIRED_PACKAGES,
    include_package_data=True,
    license='Apache 2.0',
    classifiers=[
        # Trove classifiers
        # Full list: https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    test_suite='test',
    tests_require=REQUIRED_PACKAGES
)
