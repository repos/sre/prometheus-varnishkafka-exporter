.PHONY: run build test clean test-versions

run:
	python prometheus_varnishkafka_exporter

build:
	python setup.py build

clean:
	python setup.py clean --all
	sudo find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete
	sudo rm -rf .eggs
	sudo rm -rf *.egg-info

test:
	python -m unittest

test-versions:
	make clean
	docker run -it -v $$(pwd):/src -w /src python:3.5-stretch /src/scripts/test-and-clean.sh
	docker run -it -v $$(pwd):/src -w /src python:3.6-stretch /src/scripts/test-and-clean.sh
	docker run -it -v $$(pwd):/src -w /src python:3.7-stretch /src/scripts/test-and-clean.sh
